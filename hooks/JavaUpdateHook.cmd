@echo off

rem set | grep HG

if %HG_ERROR%==1 goto final

set HG_PATH=%HG:\hg.exe=%
set HG_PATH=%HG_PATH:\hgtk.exe=%
set HG_PATH=%HG_PATH:\thgw.exe=%

for /F %%i in (preupdate_id.tmp) do java -jar "%HG_PATH%"\hg_timestamp_update.jar %%i %HG_PARENT1% 1

:final
del preupdate_id.tmp
