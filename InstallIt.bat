@echo off
if EXIST "C:\Program Files (x86)\TortoiseHg\hg.exe" set HG_ROOT="C:\Program Files (x86)\TortoiseHg"
if EXIST "C:\Program Files (x86)\Mercurial\hg.exe"  set HG_ROOT="C:\Program Files (x86)\Mercurial"

if EXIST "C:\Program Files\TortoiseHg\hg.exe" set HG_ROOT="C:\Program Files\TortoiseHg"
if EXIST "C:\Program Files\Mercurial\hg.exe"  set HG_ROOT="C:\Program Files\Mercurial"

if NOT DEFINED HG_ROOT goto NO_HG_ROOT

copy hg_timestamp_update.jar        %HG_ROOT%\
copy hooks\Java*Hook.cmd            %HG_ROOT%\
copy style\hg_timestamp_files.style %HG_ROOT%\templates\

rem Test it like this:
rem java -jar hg_timestamp_update.jar 0 tip 2

goto :EOF

:NO_HG_ROOT
echo *** Please set HG_ROOT environment variable to your hg.exe directory before installing ***
