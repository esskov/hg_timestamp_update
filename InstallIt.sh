#! /bin/sh

# What would be the best locations on a typical Linux installation?
if [ -d "/usr/share/mercurial" ]; then
	cp hg_timestamp_update.jar        /usr/share/mercurial/
	cp hooks/Java*Hook.sh             /usr/share/mercurial/
	cp style/hg_timestamp_files.style /usr/share/mercurial/templates/
else
	echo "Did not find /usr/share/mercurial - please edit the install script to fit your installation."
fi

# If mercurial was installed via a python egg, the following is necessary - but how do we  
# detect which 'templates' directory hg will look in?
for templ_dir in /usr/local/lib/python*/dist-packages/mercurial-*-linux-i686.egg/mercurial/templates ; do
	if [ -d "$templ_dir" ]; then
		cp style/hg_timestamp_files.style "$templ_dir"
	fi
done


# Test it like this:
# java -jar /usr/share/mercurial/hg_timestamp_update.jar 0 tip 2

