#! /bin/sh

# Script to help testing ...

# Make a bundle of the test_repo
# hg -R test_repo bun --all test_repo.bundle

# Extract test_repo if it doesn't exist
if [ ! -d "test_repo" ]; then
	hg clone test_repo.bundle test_repo
	cp hgrc-test_repo test_repo/.hg/hgrc
fi

# Display time-stamp of files in repo at different revisions
for test_rev in 0 1 2 3 4 5 6 7 8 9 10 11 10 9 8 7 6 5 4 3 2 1 0 ; do
	echo "--- $test_rev ---"

	hg -R test_repo up -c -r $test_rev
	hg -R test_repo sum | grep -v commit: | grep -v update:

    echo
	#find test_repo -path test_repo/.hg -prune -o -type f -printf "%f:%p:%T+\n"
	find test_repo -path test_repo/.hg -prune -o -type f -printf "%15P:%Tx\n"

    echo
done
