#! /bin/sh

# Compile and package:

if [ ! -d "classes" ]; then mkdir classes; fi
javac -d classes -sourcepath src src/hg_timestamp.java
jar cvfm hg_timestamp_update.jar src/MANIFEST.MF -C classes .

