/*
 * hg_timestamp -- an update-hook for Mercurial which can set the
 * modification-time of each updated file to that of its last revision.
 *
 * http://bitbucket.org/esskov/hg_timestamp_update
 *
 * Copyright 2009 Esben Skovenborg
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
//import com.Ostermiller.util;

class hg_timestamp {

	static final String version_string = "0.53";
	static final String style_name = "hg_timestamp_files.style";

	int verbose = 1;  // [0..3]
	long startTime;
	int hg_calls = 0;
	int revs = 0;
	SimpleDateFormat sdf;

	public static void main(String args[]) throws UnsupportedEncodingException, IOException, ParseException {
		if (args.length < 2) {
			System.err.println("hg_timestamp version " + version_string);
			System.err.println("Syntax: hg_timestamp <pre_update_rev> <update_rev> [<verbose_level>]");
			System.err.println("        pre_update_rev - start revision");
			System.err.println("        update_rev     - target revision");
			System.err.println("        verbose_level  - 0 = quiet, 1 = default, 2 = more info, 3 = all");
			System.exit(-1);
		}
		
		String preupdaterev = args[0];
		String updaterev = args[1];
		int verbose = 1;
		if (args.length >= 3)
			verbose = Integer.parseInt(args[2]);

		hg_timestamp tsu = new hg_timestamp(verbose);
		tsu.timestampUpdate(preupdaterev, updaterev);
		
		if (verbose >= 2) {
			System.out.printf("Elapsed time in hg_timestamp.main(): %.3f seconds; %d call(s) to hg.\n", 
				tsu.getElapsedTime(), tsu.getHgCalls());
		}
	}

	hg_timestamp(int verbose) {
		this.verbose = verbose;
		startTime = System.currentTimeMillis();
	}

	public void timestampUpdate(String preupdaterev, String updaterev) throws UnsupportedEncodingException, IOException, ParseException {

		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");  // ISO 8601
		sdf = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss' 'Z");  // corresp. to Hg's "isodatesec" filter
		// ISO-format might not be the most efficient way of passing a date from "hg log", but it's nice for debugging :)

		// hg histedit can say e.g. "12270ebd9843+"
		if (preupdaterev.endsWith("+"))
			preupdaterev = preupdaterev.substring(0,preupdaterev.indexOf('+'));
		if (updaterev.endsWith("+"))
			updaterev = updaterev.substring(0,updaterev.indexOf('+'));

		// As the 'null' revision has no common ancestor with another revision use '0' instead.
		if (preupdaterev.equals("000000000000"))
			preupdaterev = "0";

		String[] preupdaterev_id = hgIdentify(preupdaterev);
		String[] updaterev_id = hgIdentify(updaterev);
		// hgIdentify returns: [0] short form ID, [1] local rev num, [2] branch name

		// Does the update cross branches?
		boolean updateSameBranch = preupdaterev_id[2].equals(updaterev_id[2]);

		// Does the update go forward or backwards?
		boolean updateForward = ( Integer.parseInt(updaterev_id[1]) >= Integer.parseInt(preupdaterev_id[1]) );

		if (verbose >= 2)
			System.out.printf("[hg_timestamp hook] updateSameBranch = %b, updateForward = %b\n", updateSameBranch, updateForward);

		TreeMap<String, Date> fileMap;
		String rev;

		if (updateSameBranch) {

			if (updateForward) {
				rev = preupdaterev + "::" + updaterev;
				// "A DAG range, meaning all changesets that are descendants of x and ancestors of y, including x and y themselves"
			} else {
				rev = "reverse(" + updaterev + "::" + preupdaterev + ")";
			}
			fileMap = getFileMap(null, rev, true);

		} else {

			// first seed the fileMap with the affected files on the 'from' branch 
			rev = "ancestor(" + preupdaterev + "," + updaterev + ")::" + preupdaterev;
			fileMap = getFileMap(null, rev, false);

			// then get the modification-dates of the affected files on the 'to' branch 
			rev = "ancestor(" + preupdaterev + "," + updaterev + ")::" + updaterev;
			fileMap = getFileMap(fileMap, rev, true);
		}

        Date updaterev_date = hgLogDate(updaterev);

        if ((updaterev_date != null) && (! updaterev_date.equals(new Date(0)))) {

			if (verbose >= 1)
				System.out.printf("[hg_timestamp hook] processing %d revisions, with %d unique file-names...\n",
					revs, fileMap.size());

			Charset charset = Charset.defaultCharset();
			//Charset charset = Charset.forName("UTF-8");  // override default if necessary
			if (verbose >= 2)
				System.err.printf("Using charset: %s\n", charset.displayName());
	
			// Now iterate through the files, and "touch" each one
			for (Map.Entry<String, Date> entry : fileMap.entrySet()) {
				Date date = entry.getValue();
				String filename = URLDecoder.decode(entry.getKey(), charset.name());
				
				if ((date == null) || (updaterev_date.compareTo(date) < 0) && fileExists(filename)) {
					// file date in map is posterior to update rev: get the real last modified date for this file
					// or we don't have this file's date because updating across branches

					date = hgLogFileDate(updaterev, filename);
					//System.err.printf("Handling file %s with date %s\n", filename, date.toString());
				}
				
				if (date != null) {
					boolean touchRet = touchDontCreate(filename, date);
					if (verbose >= 3) {
						System.err.printf("%-50s = %s", filename, sdf.format(date).toString());
						if (!touchRet) {
							System.err.println(" *NO TOUCH*");
						} else {
							System.err.println();
						}
					}
				}
			}
		}
		
	}  // timestampUpdate


	public int getHgCalls() {
		return hg_calls;
	}

	public float getElapsedTime() {
		return (System.currentTimeMillis() - startTime) / 1000F;
	}


	String[] hgIdentify(String rev) throws IOException {
		String hg_command[] = {"hg", "id", "-i", "-n", "-b", "--rev=" + rev};

		//System.err.printf("Running %s:\n", Arrays.toString(hg_command));
		ExecHelper eh_id = ExecHelper.exec(hg_command);
		hg_calls++;
		String id_out = eh_id.getOutput();
		if (eh_id.getStatus() != 0) {
			System.err.println("Error: hg id returned code " + eh_id.getStatus() + ", and said -");
			System.err.println(eh_id.getError());
			throw new IOException("hg_timestamp error calling hg id");
		}

		return id_out.split("\\s+");   // return array of 3 Strings
	}


	Date hgLogFileDate(String updaterev, String filename) throws IOException, ParseException {
		String hg_command[] = {"hg", "log", "--rev=" + updaterev + ":null", "--limit=1",
			"--template={date|isodatesec}", filename};

		return hgLogDateExec(hg_command);
	}

	Date hgLogDate(String updaterev) throws IOException, ParseException {
		String hg_command[] = {"hg", "log", "--rev=" + updaterev, "--limit=1",
			"--template={date|isodatesec}"};

		return hgLogDateExec(hg_command);
	}

	Date hgLogDateExec(String[] hg_command) throws IOException, ParseException {
		//System.err.printf("Running %s:\n", Arrays.toString(hg_command));
		ExecHelper eh = ExecHelper.exec(hg_command);
		hg_calls++;
		String dateString = eh.getOutput();

		if ((dateString != null) && (dateString.length() > 0)) {
			return sdf.parse(dateString);
		} else {
			return null;
		}
	}


	TreeMap<String, Date> getFileMap(TreeMap<String, Date> fileMap, String rev, boolean useDate) throws IOException, ParseException {

		String hg_command[] = {
				"hg", "log",
				"--rev=" + rev,
				"--style=" + style_name};

		if (verbose >= 2) {
			System.err.printf("Running %s:\n", Arrays.toString(hg_command));
		}

		// Runtime.exec() may hang if the process' stdout/stderr are not read concurrently.
		// Therefore we use: http://ostermiller.org/utils/doc/com/Ostermiller/util/ExecHelper.html
		ExecHelper eh = ExecHelper.exec(hg_command);
		hg_calls++;

		if (verbose >= 2) {
			System.err.println("... returned code: " + eh.getStatus());
		}
		if (eh.getStatus() != 0) {
			System.err.println("Error: hg log returned code " + eh.getStatus() + ", and said -");
			System.err.println(eh.getError());
			throw new IOException("hg_timestamp error calling hg log");
		}

		StringReader sr = new StringReader(eh.getOutput());
		BufferedReader br = new BufferedReader(sr);

		// Key: file-name with path
		// Value: date representing milliseconds since the epoch (00:00:00 GMT, January 1, 1970)
		if (fileMap == null) {
			fileMap = new TreeMap<String, Date>();
		}

		String line1, line2;
		Date date_time = null;
		while ((line1 = br.readLine()) != null) {
			//System.err.printf("Line1: '%s'\n",line1);
			revs++;

			int ix1 = line1.indexOf('|');
			String rev_id = line1.substring(0, ix1);  // currently just for diagnostics
			if (useDate) {
				date_time = sdf.parse(line1.substring(ix1 + 1));
			}

			while ((line2 = br.readLine()) != null) {
				//System.err.printf("Line2[%d]: '%s'\n",line2.length(),line2);
				if (line2.length() == 0) {
					break;  // next revision, or done
				}
				String filename = line2;
				fileMap.put(filename, date_time);
			}
		}
		
		return fileMap;
	}


	static boolean fileExists(String filename) {
		File f = new File(filename);
		return (f.exists() && f.isFile());
	}

	static boolean touchDontCreate(String filename, Date dateTime) {
		File f = new File(filename);
		if (!(f.exists() && f.isFile())) {
			return false;
		}
		return f.setLastModified(dateTime.getTime());
	}
}
