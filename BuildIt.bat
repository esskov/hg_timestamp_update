rem Compile and package:

IF NOT EXIST classes mkdir classes

javac -d classes -sourcepath src src\hg_timestamp.java
jar cvfm hg_timestamp_update.jar src\MANIFEST.MF -C classes .
